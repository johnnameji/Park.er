from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Profile(models.Model): 
    """
    Linked to the Default Django User model and provides extra data, like watchlist, domains etc.
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    
    # General Personal Info
    country = models.CharField(max_length=30, blank=True)
    #bio = models.TextField(max_length=500, blank=True)
    #city = models.CharField(max_length=30, blank=True)
    #birth_date = models.DateField(null=True, blank=True)

    # Park Specific
    def getOwnedDomains(self):
        return self.owned_domains # this variable was created via the related_name in domains.models 

    def getWatchedDomains(self):
        return self.watched_domains

    def __str__(self):
        return self.user.username

@receiver(post_save, sender=User) 
def create_or_update_user_profile(sender, instance, created, **kwargs): #pylint: disable=W0613
    """ this *automatically* updates the profile instance whenever the user object is updated """
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()

"""
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
"""
