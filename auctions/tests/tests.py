from django.test import TestCase
from django.urls import reverse
# from ..views import xyz
# from ..forms import xzy etc.


# Create your tests here.
class AuctionListViewTests(TestCase):
    def test_no_404(self):
        """
        If no questions exist, an appropriate message is displayed.
        """
        response = self.client.get(reverse('auctions_home'))
        self.assertEqual(response.status_code, 200)
        #self.assertContains(response, "No polls are available.")
        #self.assertQuerysetEqual(response.context['latest_question_list'], [])