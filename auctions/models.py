from django.db import models
from django.utils import timezone
import datetime

from domains.models import Domain
from users.models import Profile

"""
TODO:   Test bid sorting
        Add more advanced views
        Check for security, race conditions, validation, decorators etc.
        Make sure you can't insert weird data (start/endtime created with earlier dates or mismatched)
        Add bootstrap and static stuff
        The d is silent alskdfjlaksdfa
"""

"""
    The process of domain auctioning

    1. (optional) Trends analyzed and list of popular TLD and labels for domain names are inserted into a table
    2. Dropcatching bot designates/backorders popular domains 
    3. The complete domain name is sent to park as a view to be inserted with secure credentials ex. park.er/domains/add/trove.io
    4. (optional) Another API or method is used to fill in the gaps in data for the domain info (the owner, the expiration date etc.)
    5. A domain object is created, 
        backordering enabled, if we snag the domain and only 1 backorder, they buy it outright
        If we snag it and there's more than 1 person who wants it:
            we create an auction object and sync it to the domain object
    6. The auction's start date will automatically be added once the domain is snagged (via some callback on our bot)
    7. The auction's end time will be 10 days after the start date every time. 
    8. We will use django-celery or equivalent to schedule our auction events (start and finish)
        If we get any bids in the last hour, the auction's end_time will be extended by two hours, but only the first time
        
    9. After the auction concludes, we will email all the bidders whether or not they won and the optional details if they did win
    10. At this point, the domain will have to be transferred over to them***

        We will most likely need some sort of automated email or service to send to the agency/govt/corporation 
        that owns the TLD and try to automate this process as much as possible. Domains on GoDaddy
        take some time to transfer and this might be due to external reasons, so we will have to research this.

        We should also verify that the Profile enters their full name, address and other info needed for the domain on 
        a form after winning the bid (secure ofc) if it hasn't been added already. They can add all this prior to 
        purchase on their profile page, but this will not be optional once they've purchased are are redeeming their
        domain. 

        We also will have to make sure that stripe or paypal has validated their purchase and we have received the 
        money before any details are sent to them or they receive their domain. Worst case scenario, we go to the next
        highest bidder and have them win. The auction will not be recreated; we will just keep going down the list of bids
        until someone pays up. The only exception is if there are no remaining bidders we can recreate the auction at a later time



"""

# Create your models here.
class Auction(models.Model):
    """ Tied to a unique domain name and provides all the database support for auctions """
    date_created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    last_updated = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    start_time = models.DateTimeField("Auction Start Time", null=True, blank=True) # change to null later on (for actual auction scheduling)
    end_time = models.DateTimeField('Auction End Time', null=True, blank=True)
    #last_updated = models.DateTimeField(auto_now_add=True)

    views = models.PositiveIntegerField(default=0)
    domain = models.OneToOneField(Domain, null=True, blank=True, related_name='auction', on_delete = models.CASCADE)

    def __str__(self):
        return self.domain.name

    def getTimeFancy(self):
        """ returns the time in a more user-friendly way"""
        return 0

    def hasStarted(self):
        """whether the auction has started or not (includes past auctions)"""
        now = timezone.now()
        return self.start_time and self.end_time and now >= self.start_time
    
    def hasFinished(self):
        """whether the auction has ended or not"""
        now = timezone.now()
        return self.hasStarted() and self.end_time < now

        
    #def canBid(self, Profile, amount): goes in views
         
    
    # def beginAuction(self): probably put this in a view
    
    def getCurrentPrice(self):
        """Finds the highest bid's price"""
        return self.bids.order_by('-price').first().price ## ***if using a filter and an order_by make sure that you use order_by first!!! ex. Domain.objects.order_by('-name').filter(for_sale=True)[:2]

    def isEndingSoon(self):
        """whether or not the auction is ending in less than 3 days"""
        now = timezone.now()
        isForSale = self.domain.for_sale and self.hasStarted() and not self.hasFinished()
        if not isForSale: return False
        closePeriod = self.end_time - datetime.timedelta(days=3)

        return closePeriod <= now  

class Bid(models.Model):
    """ the model for placing bids on an auction object """
    created = models.DateTimeField(auto_now_add=True)
    # Profile = ...
    price = models.PositiveIntegerField(default=0)
    auction = models.ForeignKey(Auction, related_name='bids', on_delete = models.CASCADE) #this is how we access self.bids in Auction
    profile = models.ForeignKey(Profile, null=True, related_name='bids', on_delete = models.CASCADE) # all foreign keys need a related_name (I think), just set it the same 
    
