from django.shortcuts import render, get_object_or_404
from .models import Auction
from django.views.generic import ListView

# Create your views here.
class AuctionListView(ListView):
    model = Auction
    context_object_name = 'auctions'
    template_name = 'auctions_home.html'

def auction_detail(request, pk):
    auction = get_object_or_404(Auction, pk=pk)
    return render(request, 'view_auction.html', {'auction': auction})

"""
class AuctionListView(ListView):
    model = Auction
    context_object_name = 'auctions'
    template_name = 'auctions_list.html'
    paginate_by = 20

    def get_context_data(self, **kwargs):
        session_key = 'viewed_topic_{}'.format(self.topic.pk)
        if not self.request.session.get(session_key, False):
            self.views += 1
            self.save()
            self.request.session[session_key] = True
        kwargs['topic'] = self.topic
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        self.topic = get_object_or_404(Topic, board__pk=self.kwargs.get('pk'), pk=self.kwargs.get('topic_pk'))
        queryset = self.topic.posts.order_by('created_at')
        return queryset
"""