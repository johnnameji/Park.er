# Generated by Django 2.0.6 on 2018-06-14 19:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('auctions', '0004_auto_20180614_1526'),
    ]

    operations = [
        migrations.AlterField(
            model_name='auction',
            name='domain',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='auction', to='domains.Domain'),
        ),
    ]
