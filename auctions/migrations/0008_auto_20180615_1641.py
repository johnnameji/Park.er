# Generated by Django 2.0.6 on 2018-06-15 20:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auctions', '0007_auto_20180615_1338'),
    ]

    operations = [
        migrations.AddField(
            model_name='auction',
            name='date_created',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='auction',
            name='last_updated',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
    ]
