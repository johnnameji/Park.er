#SETUP

## Enter the following commands in windows cmd:
*   pip install virtualenv
    pip install virtualenvwrapper
*   pip install virtualenvwrapper-win
*   mkvirtualenv park_env
*   pip install -r requirements.txt

## Close cmd, Open virt_env_shell and type this in: (DO THIS EVEN IF DJANGO INSTALLED)
*   IF RUNNING ON LINUX RUN 'Python3'
*   git clone https://github.com/django/django.git
*   pip install -e django/
*   (verify with): python -m django --version
*   python manage.py migrate
*   python manage.py createsuperuser
    *       Username: admin
    *       Email: teamtyro7@gmail.com
    *       Password: DJMoney711!
    

## Open VS Code and Do the following
* 1. download the python extention and the django template extension
* 2. press ctrl + shift + p and type "Terminal: Select default shell" and select powershell
* 3. Then, in that same area, type "python: select interpreter" and click the dialog box
* 4. You should see "Python 3.6.5 (virtualenv) after a small delay, click that
* 5. Next, make a blank file in the repo and call it test.py, open it in code
* 6. Now, to make sure it's working, you need to ctrl + shift + p again
       and then type "Python: run python file in terminal" while the test.py file
       is selected
* 7. You should see Python 3.6.5 (virtualenv) in the bottom left purple status bar and also (park_env) prefix in your terminal.
* 8. Now, you want to click the cog in the bottom left corner, and go to settings. Then, add the following lines:
    *       "python.venvPath": "~/Envs",
    *       "terminal.integrated.shell.windows": "C:\\WINDOWS\\System32\\WindowsPowerShell\\v1.0\\powershell.exe",
    *       "terminal.integrated.shellArgs.windows": [
                "-NoExit", "-Command", "cd $Env:HOMEPATH\\Envs\\park_env\\Scripts; .\\activate.ps1"
            ]
* 9. Now reboot code and make sure you still see Python 3.6.5(virtualenv) in purple status bar AND you also see (park_env) prefix and the drop down in the terminal box to the right says "1:powershell"
* 10. Make sure you DO NOT commit the testfile (test.py) just delete that shit and you're good.

## Notes
*   I made you a file to test the server with so it doesnt possess all of your terminal processing, just double click it when you want to test and close when you're done.

*   I made a file for using the shell outside of VS Code if you want also.

*   Also, The way I set up the virtual environments is plug and play so you can install with these settings anywhere.

*   The only requirement right now is that the virtualenv is called "park_env" for now, you can adjust this later by updating the setting in step 8 and by creating a new virtualenv using: "mkvirtualenv [env_name]"

*   All of our virtualenvs will be stored in your user directory in the \Envs\ folder

## Extra Info
*   You can find a list of commands here (WINDOWS): https://pypi.org/project/virtualenvwrapper-win/

*   AND here (bash): https://virtualenvwrapper.readthedocs.io/en/latest/command_ref.html

*   Just use the windows (powershell) wrapper for now though, we only need to use bash for when we publish

*   If you really want to use bash you can find the equivalent setting for the powershell vscode launch arg i used to start User/Envs/../Scripts/activate.ps1 and change use the bash version (Scripts/activate). I'm not going to help you with that though, setting this shit up and automatically loading it on startup for powershell was a bitch.

*   If you want more info on stuff you can do with virtualenvs on VS Code check this out: https://code.visualstudio.com/docs/python/environments

*   The reason you had to reinstall django in the virtualenv prompt is because all of our libraries we install with pip and implementations (while virtualenv is running) are installed in a new location: USER/Envs/park_env/... and this is the main purpose of virtualenvs; to make a location where all our stuff is for easier use and management.

    
## Goals
Week of May 13th:
* Build bot that checks the avaiability of the domain (John)
* Create another bot that searches multiple countries and using the dictionary of that countries language finds high value domains (John)
* Build one bot that passes through the api to check if they are trademarked and place inside a textfile/database (John)
* Build one bot that searches for expiring and new domains (John)

Week of May 20th:
* Make structural code in Django 
* Models, views etc. 
* Build another bot that buys the domain using the textfile (John)
* Once the basics are working, begin work on the login system 
    * User accounts, google, copy what park.io does 
    * Update database for user object: 
        * Id, display name, password, email etc. 

Week of May 27th:
* Test with fake domain names made in django shell 
* Create a template for the front-end 

Week of June 3rd:
* Once users tested, begin to make a user control panel 
    * Manage domains, sell/update/buy new etc. 

* Make sure the core system works 
    * Domain pool 
    * Users / Login  
        * Control panel 

* Make auctions for buying domains from dom pool 
    * Also, backordering 

Week of June 10th:
* Add a support page bot / form 
* Test everything and add support for live domains, removing test domains and users 
* Buy a web server 
    * Use webconsole, apache and tests to integrate our code and make sure it works
* Create a list of comparables for the website
    * Use NameBio, or some other equivalent

## Targeted Domain Extensions
* .app
* .io
* .ly
* .sh
* .link
* .us
* .me
* .bot
* .coin
* .in

## Stack
* Bootstrap
* Django
* Stripe for payments
* Zapier for automation 
* Mailchimp for Mailing List 
* Baremetrics for financial metrics 
* Stunning for Receipts and Failed Payments 

## Resources
* https://docs.djangoproject.com/en/2.0/topics/install/#installing-official-release
* https://tutorial.djangogirls.org/en/css/
* https://django-bootstrap3.readthedocs.io/en/latest/

## Wiki
* https://park.io
* https://adwords.google.com
* https://namebio.com
* https://www.hover.com/blog/find-out-domain-name-value/
* https://www.webhostingtalk.com/showthread.php?t=173432
* https://www.indiehackers.com/interview/73da9c0b51
* https://www.registercompass.com/wiki/domains/backorder.aspx
* http://pixelmade.com/blog/domain-names/how-buy-expired-domain-name
* https://www.domainsherpa.com/how-to-grab-an-expiring-domain-name/
* https://www.godaddy.com/help/what-happens-after-domain-names-expire-6700
* https://www.name.com/support/articles/205934737-Placing-a-backorder-on-a-domain
* https://www.domainsherpa.com/domain-name-backorder-services/
* https://indiehackers.com 
* https://baremetrics.com/open-startups 
* https://basecamp.com 

* https://hackernoon.com/how-i-built-a-profitable-saas-web-app-from-idea-to-first-sales-782efb19d900 
* https://marketingbullets.com/archive/ 
* https://www.reddit.com/r/Entrepreneur/comments/42botq/in_september_2014_we_set_a_goal_to_build_a/ 
* https://threadling.com/drip-slow-launch/ 
* https://www.devwalks.com/lets-build-a-saas-startup-1/ 
* https://devblast.com/b/7-lessons-i-learn-while-creating-my-new-saas-yoursetup-io 
* https://blog.kissmetrics.com/selling-saas-like-socrates/ 
* https://www.groovehq.com/blog/popular 
* https://www.hotjar.com/grow-your-saas-startup/ 
* http://inside.idonethis.com/startup-guide/ 
* https://justinjackson.ca/saas/ 
* https://tylertringas.com/ 
* https://hackernoon.com/how-i-built-a-profitable-saas-web-app-from-idea-to-first-sales-782efb19d900 
* https://kopywritingkourse.com/copywriting-courses-books-review/ 
* https://marketingbullets.com/available-on-dvds-for-the-first-and-only-time/ 
* https://marketingbullets.com/archive/ 

* https://docs.djangoproject.com/en/2.0/topics/db/queries/
* https://docs.djangoproject.com/en/2.0/ref/django-admin/
* https://docs.djangoproject.com/en/2.0/topics/http/views/
* https://docs.djangoproject.com/en/2.0/ref/request-response/#django.http.HttpRequest
* https://virtualenv.pypa.io/en/stable/userguide/
* https://pypi.org/project/virtualenvwrapper-win/
* https://www.digitalocean.com/community/tutorials/how-to-deploy-a-local-django-app-to-a-vps
* https://realpython.com/python-virtual-environments-a-primer/#why-the-need-for-virtual-environments
* https://docs.djangoproject.com/en/2.0/intro/install/
* https://code.visualstudio.com/docs/python/environments
* https://virtualenv.pypa.io/en/stable/installation/
* https://virtualenvwrapper.readthedocs.io/en/latest/install.html#shell-startup-file
* https://github.com/tchapi/markdown-cheatsheet/blob/master/README.md
* https://www.eurodns.com/whois-search/ai-domain-name
* https://en.wikipedia.org/wiki/.ai
* https://www.nic.io/search.htm
* http://www.icb.co.uk/
* https://iwantmyname.com/domains/new-gtld-domain-extensions
* https://www.icann.org/resources/pages/register-domain-name-2017-06-20-en
* https://www.quora.com/Where-does-GoDaddy-com-buy-domain
* http://www.networksolutions.com/support/what-is-a-domain-name-server-dns-and-how-does-it-work/
* https://docs.djangoproject.com/en/2.0/topics/http/shortcuts/#django.shortcuts.get_object_or_404
* https://docs.djangoproject.com/en/2.0/topics/testing/tools/#django.test.LiveServerTestCase
* https://docs.djangoproject.com/en/2.0/topics/testing/advanced/#topics-testing-code-coverage
* https://docs.djangoproject.com/en/2.0/topics/testing/
* https://simpleisbetterthancomplex.com/2015/11/23/small-open-source-django-projects-to-get-started.html
* https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django
* https://news.ycombinator.com/item?id=15647232

* More Tutorials: https://simpleisbetterthancomplex.com/series/beginners-guide/1.11/
* Here is the github for it: https://github.com/sibtc/django-beginners-guide.git
* Login system creation: https://simpleisbetterthancomplex.com/tutorial/2016/06/27/how-to-use-djangos-built-in-login-system.html
* Extending objects in Django: https://stackoverflow.com/questions/2181039/how-do-i-extend-the-django-group-model
* https://stackoverflow.com/questions/2181039/how-do-i-extend-the-django-group-model
* VS Code Productivity Tips: https://www.makeuseof.com/tag/10-essential-productivity-tips-visual-studio-code/


* https://howtostartablogonline.net/how-much-does-a-domain-name-cost/
* https://www.kickstartcommerce.com/premium-domain-name-values-cost.html
* http://www.businessinsider.com/why-10000-for-a-domain-name-is-still-cheap-2011-4
* https://domainpunch.com/kb/droplists.php
* https://domainpunch.com/kb/webscripts.php
* https://en.wikipedia.org/wiki/Domain_drop_catching
* https://en.wikipedia.org/wiki/Domain_registration
* https://duckduckgo.com/?q=how+to+form+an+llc+in+georgia&t=canonical&ia=web
* https://en.wikipedia.org/wiki/Domain_Name_System
* https://www.namepros.com/threads/drop-catching-your-way.855991/
* https://www.namepros.com/threads/warning-expired-domains-are-risky.547793/ 
* NAME: Alessix LLC.

* http://dnmedia.com/drop-catching-script/
* https://www.namepros.com/threads/strategy-for-expiring-deleting-domains.878648/
* https://www.namepros.com/forums/expired-domains-and-expiring-domains.307/
* http://www.desktopcatcher.com/how-it-works/

## API's
* https://whoapi.com/documentation/api/whois  //WHOis API
* https://www.npmjs.com/package/google-trends-api //Google Trends API
* https://github.com/GeneralMills/pytrends //Another Google Trends API using Python

## Bot Info
* https://www.blackhatworld.com/seo/best-way-to-write-create-bots.963113/
* https://www.blackhatworld.com/seo/the-new-age-of-bot-writing-headless-web-browsers.767792/
* https://github.com/pyzzled/selenium
* https://www.joecolantonio.com/2017/09/21/headless-browser-testing-pros-cons/
* https://developers.google.com/web/updates/2017/04/headless-chrome 
