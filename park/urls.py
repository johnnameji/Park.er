"""park URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/dev/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path #, include
from django.contrib.auth import views as auth_views

from auctions import views as auction_views
from domains import views as domain_views
from users import views as user_views


urlpatterns = [
    # path("users/", include("users.urls")),
    # path("domains/", include("domains.urls")),
    # path("auctions/", include("auctions.urls")),

    # path("profile/", UserUpdateView.as_view(), name = "user_profile" ),

    # create urls, create views, implement reusable templates

    path("", domain_views.home, name='home'),
    path("signup/", user_views.signup, name='signup'),
    path("login/", auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    path("logout/", auth_views.LogoutView.as_view(), name='logout'),

    path("auctions/", auction_views.AuctionListView.as_view(), name = "auctions_home"),
    path("domains/", domain_views.DomainListView.as_view(), name = "domains_home"),

    #re_path(r'^domains/id/(?P<pk>\d+)/$', domain_views.domain_detail, name='domain_detail'),

    # instead of domains/1/ or some id number, you can access the domain with domains/trove.io or whatever
    re_path(r'^domains/(?P<name>[0-9A-Za-z.-]+)/$', domain_views.domain_detail, name='domain_detail'), 

    re_path(r'^auctions/(?P<pk>\d+)/$', auction_views.auction_detail, name='auction_detail'),

    #auctions

    #domains

    path('admin/', admin.site.urls),
]
