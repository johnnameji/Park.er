from selenium import webdriver
import time
import sys

#USE IF WINDOWS
#driver = webdriver.Chrome(executable_path='C:/Users/johnn/Downloads/bot/chromedriver.exe')
driver = webdriver.Firefox(executable_path='/home/john/Documents/Park.er/parker_bot/geckodriver')
domain_name = open('/home/john/Documents/Park.er/parker_bot/domain_names.txt', 'r').readline()

for name in domain_name:
    driver.get('https://nic.io')
    time.sleep(1)
    search_bar = driver.find_element_by_id('search-input')
    search_bar.send_keys(domain_name)
    search_button = driver.find_element_by_id('search-submit')
    search_button.click()
    time.sleep(5)
        
    output_file = open('/home/john/Documents/Park.er/parker_bot/availability_names.txt', 'wt')

    if(driver.find_element_by_id('direct-search-result').isEmpty() == True):
        file = open('availability_names.txt', 'wt')
        file.write(name + 'UNAVAILABLE')
    else:
        file=open('availability_names.txt', 'wt')
        file.write(name + 'AVAILABLE')
