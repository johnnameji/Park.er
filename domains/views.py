from django.shortcuts import render, get_object_or_404
from .models import Domain
from django.views.generic import ListView

# Create your views here.
class DomainListView(ListView):
    """ Simple ListView for Domain objects; displays all domains on domains_home.html or /domains/ in browser """
    model = Domain
    context_object_name = 'domains'
    template_name = 'domains_home.html'

def domain_detail(request, name):
    domain = get_object_or_404(Domain, name=name)
    return render(request, 'view_domain.html', {'domain': domain})

def home(request):
    """
    TODO: This method will be the index home page of park
    """
    #domain = get_object_or_404(Domain, name=name)

    # order_by().values is a VERY FAST way to retrieve specific query data
    # recent_min = Domain.objects.order_by("-name").values("name", "auction")
    #   >>> recent_min.all() = <QuerySet [{'name': 'wew.io', 'auction': 1}, {'name': 'test3.io', 'auction': None}, {'name': 'test.com', 'auction': None}]>
    #   >>> recent_min[0] = {'name': 'wew.io', 'auction': 1}
    #   >>> recent_min[0]["name"] = 'wew.io'
    recentDomains = Domain.objects.order_by('-date_created').values(
        'name', 'auction', 'for_sale' #[:175] -TODO: later on figure out how filter based on last_updated (and add it) and also get the domains dropping soon
    ) 
    
    # 3 columns x 27 rows = 81
    sold = recentDomains.filter(for_sale=False)[:81] #only things sold will = False; anything not dropcaught
    
    # first we get all the ones that haven't sold
    forSale = recentDomains.filter(for_sale=True)

    # 3 columns x 17 rows = 51
    newAuctions = forSale.filter(auction__isnull=False)[:51]

    # 7 columns x 5 rows = 35
    droppingSoon = forSale.filter(caught=False)[:35] #could've used auction__isnull=True but this ensures I actually set this var later

    return render(request, 'home.html', {'sold': sold, 'auctions': newAuctions, 'dropping': droppingSoon})#){'domain': domain})