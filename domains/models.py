from django.db import models
from users.models import Profile
# Create your models here.

class Domain(models.Model):
    # PUBLIC
    name = models.CharField(max_length=93, unique=True)     # wew.io    
    expires = models.DateTimeField(null=True)   
    registered = models.DateTimeField(null=True)            #the actual domain registration for the customer
    for_sale = models.BooleanField(default = True)          # dropcaught, backordered/auctioned and purchased
    
    #price = models.PositiveIntegerField(default=0)

    # PRIVATE
    caught = models.BooleanField(default = False)           # whether the domain was drop-caught
    owner = models.ForeignKey(Profile, null=True, related_name='owned_domains', on_delete = models.CASCADE) # ForeignKey: for one-to-many relationships
    watching = models.ManyToManyField(Profile, related_name='watched_domains' ) # for many-to-many relationships; many domains to many users *USE SPARINGLY*
    last_updated = models.DateTimeField(auto_now_add=True, null=True, blank=True)  # updated when we change the status
    date_created = models.DateTimeField(auto_now_add=True, null=True, blank=True)  # the date we actually added the domain to our database
    #bids = {}
    

    def __str__(self): #default string for shell
        return self.name

    def getTLD(self):
        """ returns the top level domain ex. .com, .io"""
        dot = self.name.find(".")
        return self.name[dot:len(self.name)]
    
    def getLabel(self):
        """ returns the domain label ex: wee instead of wee.io"""
        dot = self.name.find(".")
        return self.name[0:dot]

    def awaitingDropCatch(self):
        """ if the domain is for sale and the auction hasn't started """
        return self.for_sale and hasattr(self.auction) and not self.auction.hasStarted() 

    # Auction Methods
    
    
    
