astroid==1.6.3
autopep8==1.3.5
colorama==0.3.9
dj-database-url==0.4.2
Django==2.0.6
django-widget-tweaks==1.4.1
isort==4.3.4
lazy-object-proxy==1.3.1
Markdown==2.6.9
mccabe==0.6.1
pycodestyle==2.4.0
pylint==1.9.2
pylint-django==0.11.1
pylint-plugin-utils==0.2.6
python-decouple==3.1
pytz==2018.4
selenium==3.12.0
six==1.11.0
virtualenv==15.2.0
virtualenvwrapper-win==1.2.5
wrapt==1.10.11
